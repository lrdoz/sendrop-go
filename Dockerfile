############################
# STEP 1 build application
############################
FROM golang:1.12-alpine as builder

RUN apk add --no-cache git gcc musl-dev

# Set the Current Working Directory inside the container
WORKDIR /app/go-sample-app

# We want to populate the module cache based on the go.{mod,sum} files.
COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

# Build the Go app
RUN go build -a -ldflags "-linkmode external -extldflags '-static' -s -w" -o /out/sendrop-go .


############################
# STEP 2 Executable image
############################
FROM scratch

COPY --from=builder /out/sendrop-go /go/bin/sendrop-go
COPY  src/public ./src/public

EXPOSE 8081

ENTRYPOINT ["/go/bin/sendrop-go"]

# Run the binary program produced by `go install`
CMD ["/go/bin/sendrop-go"]