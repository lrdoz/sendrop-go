package main

import (
	"log"
	"net/http"
	"sendrop-go/src/routes"
	"sendrop-go/src/services"
	"time"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

/**********************
***		Databse		***
***********************/
var db *gorm.DB
var err error

/**********************
***		Request		***
***********************/
func handleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)

	initStaticPageHandle(myRouter)

	myRouter.HandleFunc("/", routes.UploadFile).Methods("POST")
	myRouter.HandleFunc("/{urlFile}", routes.DownloadFile).Methods("GET")
	myRouter.HandleFunc("/{urlFile}", routes.DeleteFile).Methods("DELETE")
	log.Fatal(http.ListenAndServe(":8081", myRouter))
}

func initStaticPageHandle(myRouter *mux.Router) {
	myRouter.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./src/public/index.html")
	}).Methods("GET")
	myRouter.HandleFunc("/app.js", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./src/public/app.js")
	}).Methods("GET")
	myRouter.HandleFunc("/style.css", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./src/public/style.css")
	}).Methods("GET")
}

func main() {
	services.InitialDbConnection()
	defer services.CloseDbConnection()

	//Start Go routine for remove expired file
	go func() {
		for {
			services.RemoveExpiredFiles()
			time.Sleep(6 * time.Second)
		}
	}()

	handleRequests()
}
