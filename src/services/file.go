package services

import (
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	entities "sendrop-go/src/entities"
	"time"

	"github.com/dchest/uniuri"
)

func SaveFile(userID string, filename string, fileType string, reader_stream io.Reader) (*entities.File, error) {
	//Init information file
	file := new(entities.File)
	file.UserID = userID
	file.Filename = filename
	file.Link = uniuri.New()
	file.Type = fileType
	file.ExpirationDate = time.Now().Add(time.Hour * 60)

	//Save Uploaded file on tmp directory system
	pathDir := filepath.Join(os.TempDir(), userID)
	if err := os.MkdirAll(pathDir, os.ModePerm); err != nil {
		fmt.Printf("Error when try to create user directory in temp system : %s\n", err.Error())
		return nil, fmt.Errorf("error when try to create directory for userID")
	}

	tempFile, _ := ioutil.TempFile(pathDir, file.Link)
	defer tempFile.Close()

	fileBytes, err := ioutil.ReadAll(reader_stream)
	if err != nil {
		fmt.Printf("Error when try to read stream data : %s\n", err.Error())
		return nil, fmt.Errorf("error when try to read stream data transmitted")
	}

	writer := gzip.NewWriter(tempFile)
	defer writer.Close()

	if _, err := writer.Write(fileBytes); err != nil {
		fmt.Printf("Error when try to save file in temp system : %s\n", err.Error())
		return nil, fmt.Errorf("error when to save data transmitted")
	}

	//Save file data on database
	file.Size = len(fileBytes)
	file.PathFile = tempFile.Name()
	result := database.Create(file)
	if result.Error != nil {
		fmt.Printf("Error when try to save data information in database : %s\n", result.Error.Error())
		return nil, fmt.Errorf("error when try to save file data transmitted")

	}
	fmt.Printf("Save file transmitted by userID=%s \n", file.UserID)

	return file, nil
}

func RemoveFile(urlFile string) (*entities.File, error) {
	var file entities.File
	if err := database.Where("link = ? AND remove = false", urlFile).First(&file).Error; err != nil {
		fmt.Printf("Error when try to find file in database : %s\n", err.Error())
		return nil, fmt.Errorf("no found file")
	}

	//Remove file in temp system and flag to remove in database
	os.Remove(file.PathFile)
	file.Remove = true
	database.Save(file)

	fmt.Printf("Remove file %s transmitted by userID=%s \n", file.Link, file.UserID)
	return &file, nil
}

func RetrieveFile(urlFile string) (*entities.File, error) {
	var file entities.File
	if err := database.Where("link = ? AND remove = false", urlFile).First(&file).Error; err != nil {
		fmt.Printf("Error when try to get file in database : %s\n", err.Error())
		return nil, fmt.Errorf("no found file")
	}

	fmt.Printf("Retrieve userId=%s file\n", file.UserID)
	return &file, nil
}

func RemoveExpiredFiles() error {
	var files []entities.File
	var errors []error

	if err := database.Where("remove = false and expiration_date < ?", time.Now()).Find(&files).Error; err != nil {
		fmt.Printf("Error when try to get files in database : %s\n", err.Error())
		return fmt.Errorf("no found file")
	}

	if len(files) == 0 {
		return nil
	}

	for _, file := range files {
		if err = os.Remove(file.PathFile); err != nil {
			errors = append(errors, err)
		}
		if err = database.Model(&file).Update("remove", true).Error; err != nil {
			errors = append(errors, err)
		}
	}

	fmt.Printf("Number of files remove %d\n", len(files))
	if len(errors) > 0 {
		fmt.Printf("errors happenes when remove files %s \n", errors)
		return fmt.Errorf("error happens when remove files")

	}

	return nil
}
