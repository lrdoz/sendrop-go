package services

import (
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"net"
	entities "sendrop-go/src/entities"
	"time"
)

func CheckUser(IPRequest string, fileSize int) (string, error) {
	//Find user on database
	userID := ipRequestsToUserID(IPRequest)
	var user entities.User
	result := database.Where("user_id = ?", userID).First(&user)

	// create user if not found
	if result.Error != nil {
		user.UserID = userID
		user.LastReset = time.Now()
		database.Create(&user)
	}

	//Update limit if 24h
	if user.LastReset.Before(user.LastReset.Add(24 * time.Hour)) {
		user.LastReset = time.Now()
		user.SizeUpload = 0
	}

	// Check limit
	if user.SizeUpload+fileSize > 1000000000 {
		return "", fmt.Errorf("reached upload limit")
	}

	user.SizeUpload += fileSize
	user.NbFileUpload += 1
	database.Save(&user)

	return userID, nil
}

func ipRequestsToUserID(IPRequest string) string {
	hasher := sha1.New()
	hasher.Write(net.ParseIP(IPRequest))
	return base64.URLEncoding.EncodeToString(hasher.Sum(nil))

}
