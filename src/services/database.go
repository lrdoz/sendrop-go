package services

import (
	"fmt"
	entities "sendrop-go/src/entities"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

var database *gorm.DB
var err error

func InitialDbConnection() {
	database, err = gorm.Open("sqlite3", "test.db")

	if err != nil {
		fmt.Println(err.Error())
		panic("Failed to connect to database")
	}

	//Create database schema
	database.AutoMigrate(&entities.File{})
	database.AutoMigrate(&entities.User{})
}

func CloseDbConnection() {
	database.Close()
}
