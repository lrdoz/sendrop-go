package routes

import (
	"compress/gzip"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"

	entites "sendrop-go/src/entities"
	services "sendrop-go/src/services"

	"github.com/gorilla/mux"
)

/*******************
	File routes
 *******************/

func UploadFile(responseWriter http.ResponseWriter, request *http.Request) {
	headerData, err := extractHeaderInformation(request)
	if err != nil {
		errorResponse(responseWriter, err, http.StatusBadRequest)
		return
	}

	userID, err := services.CheckUser(headerData.IPAddress, headerData.fileSize)
	if err != nil {
		errorResponse(responseWriter, err, http.StatusNotFound)
		return
	}

	newFileData, err := services.SaveFile(userID, headerData.fileName, headerData.fileType, request.Body)
	if err != nil {
		errorResponse(responseWriter, err, http.StatusTeapot)
		return
	}
	responseWriter.WriteHeader(http.StatusCreated)
	responseFile := entites.CastFileToReponseFile(newFileData, headerData.orginHost)
	defer json.NewEncoder(responseWriter).Encode(responseFile)
}

func DeleteFile(responseWriter http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	key := vars["urlFile"]
	services.RemoveFile(key)

	responseWriter.Header().Set("Content-Type", "application/json")
	response := make(map[string]interface{})
	defer json.NewEncoder(responseWriter).Encode(response)

	response["success"] = true
}

func DownloadFile(responseWriter http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	key := vars["urlFile"]

	file, err := services.RetrieveFile(key)
	if err != nil {
		errorResponse(responseWriter, err, http.StatusNotFound)
		return
	}

	//Send file on reponse content
	fileStream, _ := os.Open(file.PathFile)
	defer fileStream.Close()

	reader, _ := gzip.NewReader(fileStream)
	defer reader.Close()

	responseWriter.Header().Set("Content-Type", "application/octet-stream")
	responseWriter.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%q", file.Filename))
	io.Copy(responseWriter, reader)
}

/*******************
	Function tools
 *******************/

type headerInformation struct {
	IPAddress string
	fileName  string
	fileType  string
	orginHost string
	fileSize  int
}

func extractHeaderInformation(request *http.Request) (*headerInformation, error) {
	request.ParseForm()

	//Get Header data for upload file
	header := new(headerInformation)

	if header.IPAddress = request.Header.Get("X-Forwarded-For"); header.IPAddress == "" {
		header.IPAddress = request.RemoteAddr
	}
	header.fileName = request.Header.Get("X-Filename")
	header.fileType = request.Header.Get("Content-Type")
	header.orginHost = request.Header.Get("Origin")
	fileSize, err := strconv.Atoi(request.Header.Get("Content-Length"))
	if err != nil || header.fileName == "" {
		fmt.Printf("error on request header content-Length=%s or filename=%s", err, header.fileName)
		return nil, fmt.Errorf("error on request header")
	}
	header.fileSize = fileSize

	return header, nil
}

func initResponse(responseWriter http.ResponseWriter) map[string]interface{} {
	responseWriter.Header().Set("Content-Type", "application/json")
	response := make(map[string]interface{})
	response["success"] = true

	return response
}

func errorResponse(responseWriter http.ResponseWriter, err error, statusCode int) {

	responseWriter.Header().Set("Content-Type", "application/json")
	responseWriter.WriteHeader(statusCode)

	response := make(map[string]interface{})
	defer json.NewEncoder(responseWriter).Encode(response)

	response["success"] = false
	response["error"] = err.Error()
}
