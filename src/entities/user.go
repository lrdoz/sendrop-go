package entites

import (
	"time"

	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model
	UserID       string `gorm:"primaryKey;unique"`
	SizeUpload   int    `gorm:"default:0"`
	NbFileUpload int    `gorm:"default:0"`
	LastReset    time.Time
}
