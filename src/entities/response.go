package entites

import (
	"fmt"
	"net/url"
	"path"
	"time"
)

type sizeType struct {
	Bytes    int    `json:"bytes"`
	Readable string `json:"readable"`
}

type aliasType struct {
	Short string `json:"short"`
	Long  string `json:"long"`
}

type linkType struct {
	Short string `json:"short"`
	Long  string `json:"long"`
}

type duration struct {
	Seconds  int    `json:"seconds"`
	Readable string `json:"readable"`
}

type date struct {
	Timestamp int64  `json:"timestamp"`
	Readable  string `json:"readable"`
}

type expirationType struct {
	Duration duration `json:"duration"`
	Date     date     `json:"date"`
}

type ResponseFile struct {
	Name       string         `json:"name"`
	Size       sizeType       `json:"size"`
	Alias      aliasType      `json:"alias"`
	Link       linkType       `json:"link"`
	Expiration expirationType `json:"expiration"`
	Success    bool           `json:"success"`
}

func CastFileToReponseFile(file *File, host string) *ResponseFile {

	size := sizeType{file.Size, humanReadableByteCountSI(file.Size)}
	alias := aliasType{file.Link, file.Link}
	u, _ := url.Parse(host)
	u.Path = path.Join(u.Path, file.Link)
	link := linkType{u.String(), u.String()}
	expirationTime := time.Until(file.ExpirationDate)

	duration := duration{int(expirationTime.Seconds()), expirationTime.String()}
	date := date{file.ExpirationDate.Unix(), file.ExpirationDate.Format("2021/04/30 05:00")}
	expiration := expirationType{duration, date}

	var response *ResponseFile = new(ResponseFile)

	response.Name = file.Filename
	response.Size = size
	response.Link = link
	response.Alias = alias
	response.Expiration = expiration
	response.Success = true

	return response
}

func humanReadableByteCountSI(bytes int) string {
	floatBytes := float64(bytes)
	if floatBytes < 1000.0 {
		return fmt.Sprintf("%.1f B", floatBytes)
	}

	for _, char := range "kMGTPE" {
		if floatBytes <= 1000.0 {
			return fmt.Sprintf("%.1f %cB", floatBytes/1000.0, char)
		}
		floatBytes /= 1000.0
	}
	return ""
}
