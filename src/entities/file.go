package entites

import (
	"time"

	"github.com/jinzhu/gorm"
)

type File struct {
	gorm.Model
	UserID         string    `gorm:"not null"`
	Filename       string    `gorm:"not null"`
	Type           string    `gorm:"not null"`
	Size           int       `gorm:"not null"`
	Link           string    `gorm:"not null"`
	Remove         bool      `gorm:"default:false"`
	ExpirationDate time.Time `gorm:"not null"`
	PathFile       string    `gorm:"not null"`
}
